const xlsx = require('node-xlsx');
const path = require('path')
const config = require('../config');
const data_path = path.join(__dirname + '/../data_sheets/data.xlsx');

const file_data = xlsx.parse(data_path);
file_data[0].data.shift();
let element_income = null;
let element_operationsTypes = [];
let element_moneyOnOperatuions = null;
let counter = 0;
console.log('Data-length',file_data[0].data.length)
file_data[0].data.forEach(element => {
    if ( element[0] === config.regions[0].name) {
        counter++;
        element_income += element[2];
        let temp_obj = {
            operation_type: element[1],
            operation_money: element[2] * 10
        }
        element_operationsTypes.push(temp_obj);
        
        let obj = {
            region_name: element[0],
            region_operations: element_operationsTypes,
            region_income: element_income
        };
        if(counter === 6) {
            console.log(obj)
        }
    }
});